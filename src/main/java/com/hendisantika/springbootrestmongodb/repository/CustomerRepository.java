package com.hendisantika.springbootrestmongodb.repository;

import com.hendisantika.springbootrestmongodb.model.Customer;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-rest-mongodb
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 20/09/18
 * Time: 21.08
 * To change this template use File | Settings | File Templates.
 */
public interface CustomerRepository extends MongoRepository<Customer, String> {
    List<Customer> findByAge(int age);
}