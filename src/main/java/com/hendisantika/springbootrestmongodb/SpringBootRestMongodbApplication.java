package com.hendisantika.springbootrestmongodb;

import com.hendisantika.springbootrestmongodb.model.Customer;
import com.hendisantika.springbootrestmongodb.repository.CustomerRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@Slf4j
public class SpringBootRestMongodbApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootRestMongodbApplication.class, args);
    }

    @Bean
    CommandLineRunner initData(CustomerRepository repository) {
        return args -> {
            repository.deleteAll();
            repository.save(new Customer("Uzumaki Naruto", 20));
            repository.save(new Customer("Uchiha Sasuke", 22));
            repository.save(new Customer("Sakura Haruno", 20));
            repository.save(new Customer("Hatake Kakashi", 40));
            log.info("Data --> {}", repository.findAll());
        };
    }
}
