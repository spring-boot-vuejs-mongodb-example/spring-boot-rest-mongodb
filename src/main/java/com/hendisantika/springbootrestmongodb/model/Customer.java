package com.hendisantika.springbootrestmongodb.model;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-rest-mongodb
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 20/09/18
 * Time: 21.06
 * To change this template use File | Settings | File Templates.
 */
@Document(collection = "customer")
@Data
@AllArgsConstructor
@Setter
@Getter
@ToString
public class Customer {
    @Id
    private String id;

    private String name;
    private int age;
    private boolean active;

    public Customer(String name, int age) {
        this.name = name;
        this.age = age;
    }
}